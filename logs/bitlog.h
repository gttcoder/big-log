#ifndef __M_BITLOG_H__
#define __M_BITLOG_H__
#include "util.hpp"
#include "level.hpp"
#include "message.hpp"
#include "format.hpp"
#include "sink.hpp"
#include "logerr.hpp"
#include "buffer.hpp"
#include "looper.hpp"
namespace gttlog
{
    // 1 获取指定日志器的全局接口，避免用户自己操作单例对象
    Logger::ptr getLogger(const std::string &name)
    {
        return gttlog::LoggerManager::getInstance().getLogger(name);
    }

    Logger::ptr rootLogger()
    {
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1";
        return gttlog::LoggerManager::getInstance().rootLogger();
    }
// 2使用宏函数对日志器的接口进行代理（代理模式）
#define debug(fmt, ...) debug(__FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define info(fmt, ...) info(__FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define warn(fmt, ...) warn(__FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define error(fmt, ...) error(__FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define fatal(fmt, ...) fatal(__FILE__, __LINE__, fmt, ##__VA_ARGS__)

// 3 提供宏函数，直接通过默认日志器进行日志的标准输出打印（不用获取日志器了）
#define DEBUG(fmt, ...) gttlog::rootLogger()->debug(fmt, ##__VA_ARGS__)
#define INFO(fmt, ...) gttlog::rootLogger()->info(fmt, ##__VA_ARGS__)
#define WARN(fmt, ...) gttlog::rootLogger()->warn(fmt, ##__VA_ARGS__)
#define ERROR(fmt, ...) gttlog::rootLogger()->error(fmt, ##__VA_ARGS__)
#define FATAL(fmt, ...) gttlog::rootLogger()->fatal(fmt, ##__VA_ARGS__)
}

#endif