#include "../logs/bitlog.h"
#include <unistd.h>


void testlog(const std::string & name)
{
    INFO("%s","测试开始");
    gttlog::Logger::ptr logger = gttlog::LoggerManager::getInstance().getLogger(name);

    if(nullptr==logger)
    {
        std::cout<<"nullptr\n";
    }
    
    logger->debug("%s", "测试日志");
    logger->info( "%s", "测试日志");
    logger->warn( "%s", "测试日志");
    logger->error("%s", "测试日志");
    logger->fatal("%s", "测试日志");

    
    INFO("%s","测试结束");
   
   
}
int main()
{
    std::unique_ptr<gttlog::LoggerBuilder> builder(new gttlog::GlobalLoggerBuilder());
    builder->buildLoggerName("async_logger");
    builder->buildLoggerLevel(gttlog::LogLevel::value::DEBUG);
    builder->buildFormatter("[%c][%f:%l][%p]%m%n");
    builder->buildLoggerType(gttlog::LoggerType::LOGGER_SYNC);
    builder->buildSink<gttlog::FileSink>("./logfile/async.log");
    builder->buildSink<gttlog::StdoutSink>();
  
    builder->build();
    testlog("async_logger");
    return 0;
}