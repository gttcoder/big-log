// 扩展一个以时间作为入职文件滚动切换类型的日志落地模块
// 1 以时间进行文件滚动，实际上以时间段进行滚动
// 2 实现思想 time(nullptr) % gap
//       time(nullptr) % 60
#include "../logs/bitlog.h"
#include <unistd.h>
enum class TimeGap
{
    GAP_SECOND,
    GAP_MINUTE,
    GAP_HOUR,
    GAP_DAY
};
class RollbyTimeFileSink : public gttlog::LogSink
{
public:
    // 构造时传入文件名，并打卡文件，将操作句柄管理起来
    RollbyTimeFileSink(const std::string &basename, TimeGap gap_type)
        : _basename(basename)
    {
        switch (gap_type)
        {
        case TimeGap::GAP_SECOND:
            _gap_size = 1;
            break;
        case TimeGap::GAP_MINUTE:
            _gap_size = 60;
            break;
        case TimeGap::GAP_HOUR:
            _gap_size = 3600;
            break;
        case TimeGap::GAP_DAY:
            _gap_size = 3600 * 24;
            break;
        default:
            break;
        }
        _cur_gap = _gap_size == 1 ? gttlog::util::Date::now() : gttlog::util::Date::now() % _gap_size; // 获取当前是第几个时间段
        std::string filename = createNewfile();
        gttlog::util::File::createDiretory(gttlog::util::File::path(filename));
        _ofs.open(filename, std::ios::binary | std::ios::app);
        assert(_ofs.is_open());
    }
    // 将日志消息写入到标准输出,判断当前时间是否是当前文件的时间段，不是则切换文件
    void log(const char *data, size_t len)
    {
        time_t cur = gttlog::util::Date::now();
        if ((cur % _gap_size) != _cur_gap)
        {
            _ofs.close();
            std::string filename = createNewfile();
            _ofs.open(filename, std::ios::binary | std::ios::app);
            assert(_ofs.good());
        }
        _ofs.write(data, len);
        assert(_ofs.good());
    }

private:
    std::string createNewfile()
    {
        // 获取系统赶时间，以时间来构造文件扩展名
        time_t t = gttlog::util::Date::now();
        struct tm lt;
        localtime_r(&t, &lt);
        std::stringstream filename;
        filename << _basename;
        filename << lt.tm_year + 1900;
        filename << lt.tm_mon + 1;
        filename << lt.tm_mday;
        filename << lt.tm_hour;
        filename << lt.tm_min;
        filename << lt.tm_sec;
        filename << ".log";
        return filename.str();
    }

private:
    std::string _basename;
    std::ofstream _ofs;
    size_t _cur_gap;
    size_t _gap_size;
    TimeGap _timegap;
};
void testlog(const std::string & name)
{
    INFO("%s","测试开始");
    gttlog::Logger::ptr logger = gttlog::LoggerManager::getInstance().getLogger(name);

    if(nullptr==logger)
    {
        std::cout<<"nullptr\n";
    }
    
    logger->debug("%s", "测试日志");
    logger->info( "%s", "测试日志");
    logger->warn( "%s", "测试日志");
    logger->error("%s", "测试日志");
    logger->fatal("%s", "测试日志");

    
    INFO("%s","测试结束");
   
   
}
int main()
{
    std::unique_ptr<gttlog::LoggerBuilder> builder(new gttlog::GlobalLoggerBuilder());
    builder->buildLoggerName("async_logger");
    builder->buildLoggerLevel(gttlog::LogLevel::value::WARN);
    builder->buildFormatter("[%c]%m%n");
    builder->buildLoggerType(gttlog::LoggerType::LOGGER_ASYNC);
    builder->buildSink<gttlog::RollSink>("./logfile/async.log",TimeGap::GAP_SECOND);
    gttlog::Logger::ptr logger =  builder->build();
    size_t cur=gttlog::util::Date::now();
    while ((gttlog::util::Date::now())<cur+5)
    {
        logger->fatal("这是一条测试日志");
        usleep(1000);
    }
  
    
    //testlog("async_logger");
    return 0;
}