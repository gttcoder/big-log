#include<iostream>
#include<string>
class RentHouse
{
public:
    virtual void rentHouse()=0;
};
class Landlord:public RentHouse
{
public:
    virtual void rentHouse()
    {
        std::cout<<"将房子租出去"<<std::endl;
    }
};
class Intermediary:public RentHouse
{
public:
    virtual void rentHouse()
    {
        std::cout<<"发布信息"<<std::endl;
        std::cout<<"带人看房"<<std::endl;
        _landlord.rentHouse();
        std::cout<<"负责售后"<<std::endl;
    }
private:
    Landlord _landlord;
};
int main()
{
    Intermediary intermediary;
    intermediary.rentHouse();
    return 0;
}