#ifndef __M_UTIL_H__
#define __M_UTIL_H__
/*
    1、获取系统时间
    2、判断文件是否存在
    3、获取文件所在的目录路径
    4、创建目录
*/
#include <iostream>
#include <ctime>
#include <sys/stat.h>
namespace gttlog
{
    namespace util
    {
        class Date
        {
        public:
            static size_t now()
            {
                return (size_t)time(nullptr);
            }
        };
        class File
        {
        public:
            static bool exists(const std::string &pathname)
            {
                struct stat st;
                int n = stat(pathname.c_str(), &st);
                if (n < 0)
                {
                    return false;
                }
                return true;
            }
            static std::string path(const std::string &pathname)
            {
                //./abc/test.txt
                size_t pos = pathname.find_last_of("/\\");
                if (pos == std::string::npos)
                {
                    return ".";
                }
                return pathname.substr(0, pos + 1);
            }
            static void createDiretory(const std::string &pathname)
            {
                size_t index = 0, pos = 0;
               
                while (index < pathname.size())
                {

                    pos = pathname.find_first_of("/\\", index);
                    if (pos == std::string::npos)
                    {
                        // 没找到
                        mkdir(pathname.c_str(), 0777);
                        return;
                    }

                    std::string parentdir = pathname.substr(0, pos + 1);

                    if (exists(parentdir) == true)
                    {
                        index = pos + 1;
                        
                        continue;
                    }

                    mkdir(parentdir.c_str(), 0777);

                    index = pos + 1;
                 
                }
            }
        };
    }
}
#endif