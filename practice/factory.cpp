#include <iostream>
#include <memory>
using namespace std;

class Fruit
{
public:
    virtual void name() = 0;
};
class Apple : public Fruit
{
public:
    virtual void name() override
    {
        cout << "我是一个苹果" << endl;
    }
};
class Banana : public Fruit
{
public:
    virtual void name() override
    {
        cout << "我是一个香蕉" << endl;
    }
};

class Animal
{
public:
    virtual void name() = 0;
};
class Dog : public Animal
{
public:
    void name()
    {
        cout << "我是一个小狗" << endl;
    }
};
class Lamp : public Animal
{
public:
    void name()
    {
        cout << "我是一个山羊" << endl;
    }
};
// 简单工厂模式
//  class Factory
//  {
//      public:
//          static shared_ptr<Fruit> create(const string name)
//          {
//              if(name=="苹果")
//              {
//                  return make_shared<Apple>();
//              }
//              else{
//                  return make_shared<Banana>();
//              }
//          }
//  };
//  int main()
//  {
//      shared_ptr<Fruit> fruit=Factory::create("苹果");
//      fruit->name();
//      fruit=Factory::create("香蕉");
//      fruit->name();
//      return 0;
//  }

// 方法模式
//  class Factory
//  {
//      public:
//          virtual  shared_ptr<Fruit> create()=0;
//  };

// class AppleFactor:public Factory
// {
//     public:
//         virtual shared_ptr<Fruit> create() override
//         {
//             return make_shared<Apple>();
//         }
// };
// class BananaFactor:public Factory
// {
//     public:
//         virtual shared_ptr<Fruit> create() override
//         {
//             return make_shared<Banana>();
//         }
// };

// int main()
// {
//     shared_ptr<Factory> factory(new AppleFactor());
//     shared_ptr<Fruit> fruit=factory->create();

//     fruit->name();

//     factory.reset(new BananaFactor());
//     fruit=factory->create();
//     fruit->name();

//     return 0;
// }

// 代理模式
class Factory
{
public:
    virtual shared_ptr<Fruit> getFruit(const string &name) = 0;
    virtual shared_ptr<Animal> getAnimal(const string &name) = 0;
};
class FruitFacory : public Factory
{
public:
    virtual shared_ptr<Fruit> getFruit(const string &name)
    {
        if (name == "苹果")
        {
            return make_shared<Apple>();
        }
        else
            return make_shared<Banana>();
    }
    virtual shared_ptr<Animal> getAnimal(const string& name)
    {
        return shared_ptr<Animal>();
    }
};

class AnimalFactory : public Factory
{
public:
    virtual shared_ptr<Fruit> getFruit(const string &name)
    {
        return shared_ptr<Fruit>();
    }
    virtual shared_ptr<Animal> getAnimal(const string &name)
    {
        if (name == "山羊")
            return make_shared<Lamp>();
        else
            return make_shared<Dog>();
    }
};
class FactoryProductor
{
public:
    static shared_ptr<Factory> getFactory(const string &name)
    {
        if (name == "动物")
            return make_shared<AnimalFactory>();
        else
            return make_shared<FruitFacory>();
    }
};

int main()
{
    shared_ptr<Factory> ff=FactoryProductor::getFactory("动物");
    shared_ptr<Animal> lamp=ff->getAnimal("山羊");
    lamp->name();

    return 0;
}
