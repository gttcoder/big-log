#include <iostream>
using namespace std;

void xprintf()
{
    cout << endl;
}
// C++不定参函数使用
template <class T, class... Args>
void xprintf(const T &val, Args &&...args)
{
    cout << val;
    if (sizeof...(args) > 0)
    {
        xprintf(forward<Args>(args)...);
    }
    else
    {
        xprintf();
    }
}
int main()
{
    xprintf("龚婷婷");
    xprintf("gtt", 6666);
    xprintf("gtt", "heihei", 666);
    return 0;
}