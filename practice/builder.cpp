#include<iostream>
#include<string>
#include<memory>
using namespace std;

class Computer
{
public:
    void SetBorad(const string& borad)
    {
        _board=borad;
    }
    void SetDisplay(const string& display)
    {
        _display=display;
    }
    virtual void SetOs()=0;
    void SetParamer()
    {
        string param="Computer:\n";
        param+="\tboard:"+_board;
        param+="\tdisplay:"+_display;
        param+="\tOs:"+_Os;
        cout<<param<<endl;
    }
protected:
    string _board;
    string _display;
    string _Os;
};

class MacBook:public Computer
{
public:
    virtual void SetOs() override
    {
        _Os="Mac OS x12";
    }
};
class Builder
{
public:
    virtual void buildBoard(const string& borad)=0;
    virtual void buildDisplay(const string& display)=0;
    virtual void buildOs()=0;
    virtual shared_ptr<Computer> build()=0;
};
class MacBookBuilder:public Builder
{
    public:
        MacBookBuilder():_computer(new MacBook())
        {
        }
        virtual void buildBoard(string& borad)
        {
            _computer->SetBorad(borad);
        }
        virtual void buildDisplay(string& display)
        {
            _computer->SetDisplay(display);
        }
        virtual void buildOs()
        {
            _computer->SetOs();
        }
        virtual shared_ptr<Computer> build()
        {
            return _computer;
        }
    private:
        shared_ptr<Computer> _computer;
};

class Director{
public:
    Director(Builder* builder)
    :_builder(builder)
    {}
    void construct(const string& borad,const string& display)
    {
        _builder->buildBoard(borad);
        _builder->buildDisplay(display);
        _builder->buildOs();
    }
private:
    shared_ptr<Builder> _builder;
};
int main()
{
    Builder* builder=new MacBookBuilder();
    unique_ptr<Director> pd(new Director(builder));
    pd->construct("华硕","三星显示屏");
    shared_ptr<Computer> com=builder->build();
    com->SetParamer();
    return 0;
}