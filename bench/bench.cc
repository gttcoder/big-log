#include "../logs/bitlog.h"
#include <chrono>

void bench(const std::string &logger_name, size_t thr_count, size_t msg_count, size_t msg_len)
{
    // 1 获取日志器
    gttlog::Logger::ptr logger = gttlog::getLogger(logger_name);
    if (logger.get() == nullptr)
    {
        return;
    }
    std::cout << "测试日志：" << msg_count << "条，总大小" << msg_count * msg_len / 1024 << "KB\n";
    // 2 组织指定长度的日志消息
    std::string msg(msg_len - 1, 'A'); // 少一个字节，是为了给末尾添加换行
    // 3 创建指定数量的线程
    std::vector<std::thread> threads;
    size_t msg_pth_thr = msg_count / thr_count; // 总日志数据/线程数据就是每隔线程要输出的日志数量
    std::vector<double> cost_arry(thr_count);
    for (int i = 0; i < thr_count; ++i)
    {
        threads.emplace_back([&, i]()
                             {
                                 // 4 线程函数内部开始
                                auto start=std::chrono::high_resolution_clock::now();
                                 // 5 开始循环写日志
                                 for(int j=0;j<msg_pth_thr;j++)
                                 {
                                    logger->fatal("%s",msg.c_str());
                                 }
                                 // 6 线程函数内部结束
                                auto end=std::chrono::high_resolution_clock::now();
                                std::chrono::duration<double> cost=end-start;
                                cost_arry[i]=cost.count();
                                std::cout<<"线程"<<i<<" : "<<"\t输出日志数量："<<msg_pth_thr<<"耗时时间："<<cost.count()<<"s"<<std::endl; });
    }

    // 7 计算总耗时 耗时最长的才是最终时间
    for (int i = 0; i < thr_count; ++i)
    {
        threads[i].join();
    }
    double max_cost = cost_arry[0];
    for (int i = 0; i < thr_count; ++i)
        max_cost = max_cost < cost_arry[i] ? cost_arry[i] : max_cost;
    size_t msg_per_sec = msg_count / max_cost;
    size_t size_per_sec = (msg_count * msg_len) / (max_cost * 1024);
    // 8 进行输出打印
    std::cout<<"\t总耗时"<<max_cost<<"s\n";
    std::cout << "\t每秒钟输出日志数量：" << msg_per_sec << "条\n";
    std::cout << "\t每秒钟输出日志大小：" << size_per_sec << "KB\n";
}
void sysnc_bench()
{
    std::unique_ptr<gttlog::LoggerBuilder> builder(new gttlog::GlobalLoggerBuilder());
    builder->buildLoggerName("sync_logger");
    builder->buildFormatter("%m%n");
    builder->buildLoggerType(gttlog::LoggerType::LOGGER_SYNC);
    builder->buildSink<gttlog::FileSink>("./logfile/sync.log");
    builder->build();
    bench("sync_logger",3,1000000,100);

}
void asysnc_bench()
{
    std::unique_ptr<gttlog::LoggerBuilder> builder(new gttlog::GlobalLoggerBuilder());
    builder->buildLoggerName("async_logger");
    builder->buildFormatter("%m%n");
    builder->buildLoggerType(gttlog::LoggerType::LOGGER_ASYNC);
    builder->buildEnableUnSafaAsync();//将实际落地时间排除在外
    builder->buildSink<gttlog::FileSink>("./logfile/async.log");
    builder->build();
    bench("async_logger",1,2000000,100);

}
int main()
{
    //sysnc_bench();
    asysnc_bench();
}