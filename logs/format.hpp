#ifndef __M_FMT_H__
#define __M_FMT_H__
#include "level.hpp"
#include "message.hpp"
#include <memory>
#include <vector>
#include <sstream>
#include <cassert>
namespace gttlog
{
    // 抽象格式化子项基类
    class FormatItem
    {
    public:
        using ptr = std::shared_ptr<FormatItem>;
        virtual void format(std::ostream &out, LogMsg &msg) = 0;
    };
    // 派生格式化子项子类--消息，等级，时间，文件名，行号，线程ID，日志器名，制表符，换行，其他
    class MsgFormatItem : public FormatItem
    {
    public:
        void format(std::ostream &out, LogMsg &msg)
        {
            out << msg._payload;
        }
    };
    class LevelFormatItem : public FormatItem
    {
    public:
        void format(std::ostream &out, LogMsg &msg)
        {
            out << LogLevel::toString(msg._level);
        }
    };
    class TimeFormatItem : public FormatItem
    {
    public:
        TimeFormatItem(const std::string &fmt = "%H:%M:%S")
            : _time_fmt(fmt) {}
        void format(std::ostream &out, LogMsg &msg)
        {
            struct tm t;
            localtime_r(&msg._ctime, &t);
            char tmp[32] = {0};
            strftime(tmp, 31, _time_fmt.c_str(), &t);
            out << tmp;
        }

    private:
        std::string _time_fmt;
    };

    class FileFormatItem : public FormatItem
    {
    public:
        void format(std::ostream &out, LogMsg &msg)
        {
            out << msg._file;
        }
    };

    class LineFormatItem : public FormatItem
    {
    public:
        void format(std::ostream &out, LogMsg &msg)
        {
            out << msg._line;
        }
    };

    class ThreadFormatItem : public FormatItem
    {
    public:
        void format(std::ostream &out, LogMsg &msg)
        {
            out << msg._tid;
        }
    };

    class LoggerFormatItem : public FormatItem
    {
    public:
        void format(std::ostream &out, LogMsg &msg)
        {
            out << msg._logger;
        }
    };
    class TabFormatItem : public FormatItem
    {
    public:
        void format(std::ostream &out, LogMsg &msg)
        {
            out << "\t";
        }
    };
    class NLineFormatItem : public FormatItem
    {
    public:
        void format(std::ostream &out, LogMsg &msg)
        {
            out << "\n";
        }
    };
    class OtherFormatItem : public FormatItem
    {
    public:
        OtherFormatItem(const std::string &str)
            : _str(str) {}
        void format(std::ostream &out, LogMsg &msg)
        {
            out << _str;
        }

    private:
        std::string _str;
    };
    /*
        %d表示时间
        %t表示线程ID
        %c表示日志器名称
        %f表示源码文件名
        %l表示源码行号
        %p表示日志级别
        %T表示制表符缩进
        %m表示消息主体
        %n表示换行
    */
    class Formatter
    {
    public:
        using ptr=std::shared_ptr<Formatter>;
        Formatter(const std::string &pattern = "[%d{%H:%M:%S}][%t][%c][%f:%l][%p]%T%m%n")
            : _pattern(pattern)
        {
            
            assert(parsePattern());
        }
        // 对msg进行格式化
        std::ostream& format(std::ostream &out, LogMsg &msg)
        {
            for (auto &item : _items)
            {
                item->format(out, msg);
                //std::cout << msg << std::endl;
            }
            // std::cout << msg._ctime << std::endl;
            // std::cout << msg._file << std::endl;
            // //std::cout << msg._level << std::endl;
            // std::cout << msg._line << std::endl;
            // std::cout << msg._logger << std::endl;
            // std::cout << msg._payload << std::endl;
            // std::cout << msg._tid << std::endl;
            return out;
            
        }
        std::string format(LogMsg &msg)
        {
            std::stringstream ss;

            format(ss, msg);
            
            return ss.str();
        }

    private:
        // 对格式化规则字符串进行解析
        bool parsePattern()
        {
            // 对格式化规则字符串进行解析
            // abcd[%d{%H:%M"%S}][%p]%T%m%n
            //std::cout<<_pattern<<std::endl;
            std::vector<std::pair<std::string, std::string>> fmt_order;
            size_t pos = 0;
            std::string key, val;
            while (pos < _pattern.size())
            {
                // 处理原始字符串--判断是否是% ，不是就是原始字符
                if (_pattern[pos] != '%')
                {
                    val.push_back(_pattern[pos++]);
                    continue;
                }
                // pos位置是一个字符%,  将%%处理成一个%
                if (pos + 1 < _pattern.size() && _pattern[pos + 1] == '%')
                {
                    val.push_back('%');
                    pos += 2;
                    continue;
                }
                if (val.empty() == false)
                {

                    fmt_order.push_back(std::make_pair("", val));
                    
                    val.clear();
                }
                // 格式化字符的处理
                pos += 1;
                if (pos == _pattern.size())
                {
                    std::cout << "%之后，没有对应的格式化字符\n";
                    return false;
                }
                key = _pattern[pos];
                // 这时pos指向格式化字符后的位置
                pos += 1;

                if (pos < _pattern.size() && _pattern[pos] == '{')
                {
                    pos += 1; // pos指向{之后，子规则的起始位置
                    while (pos < _pattern.size() && _pattern[pos] != '}')
                    {

                        val.push_back(_pattern[pos++]);
                    }

                    // 如果走到了末尾，没有找到},代表格式是错误的
                    if (pos == _pattern.size())
                    {
                        std::cout << "子规则{}匹配出错\n";
                        return false; // 没有找到
                    }
                    pos += 1; // 因为这时候pos指向的是}位置，往后走一步，走到了下次需要处理的位置
                }
                fmt_order.push_back(std::make_pair(key, val));
                

                key.clear();
                val.clear();
            }
            // 根据解析得到的数据初始化格式化子项数组成员
            for (auto &it : fmt_order)
            {

                _items.push_back(createItem(it.first, it.second));
                // std::cout<<_items[_items.size()-1]<<std::endl;
            }

            return true;
        }

        FormatItem::ptr createItem(const std::string &key, const std::string &val)
        {
            if (key == "d")
                return std::make_shared<TimeFormatItem>(val);
            if (key == "t")
                return std::make_shared<ThreadFormatItem>();
            if (key == "c")
                return std::make_shared<LoggerFormatItem>();
            if (key == "f")
                return std::make_shared<FileFormatItem>();
            if (key == "l")
                return std::make_shared<LineFormatItem>();
            if (key == "p")
                return std::make_shared<LevelFormatItem>();
            if (key == "T")
                return std::make_shared<TabFormatItem>();
            if (key == "m")
                return std::make_shared<MsgFormatItem>();
            if (key == "n")
                return std::make_shared<NLineFormatItem>();
            if(key=="") return std::make_shared<OtherFormatItem>(val);
            std::cout<<"没有对应的格式胡字符:%"<<key<<std::endl;
            abort();
            return FormatItem::ptr();

        }

    private:
        std::string _pattern;
        std::vector<FormatItem::ptr> _items;
    };
}

#endif