#include <iostream>
using namespace std;

// 饿汉模式 以空间换时间
//  class Single
//  {
//      public:
//          static Single& getInstance()
//          {
//              return _eton;
//          }
//      private:
//          Single()
//          {
//              cout<<"单例模式已经生成"<<endl;
//          }
//          ~Single(){}
//          Single(const Single& s)=delete;
//          static Single _eton;
//  };
//  Single Single::_eton;

//饿汉模式
class Single
{
    public:
    static Single& getInstance()
    {
        static Single _eton;
        return _eton;
    }
private:
    Single()
    {
        cout<<"单例模式"<<endl;
    }
    ~Single()
    {}
    Single(const Single&)=delete;
    
};
int main()
{
    Single::getInstance();
    return 0;
}

