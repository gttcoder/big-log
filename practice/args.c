//不定参函数
#include<stdio.h>
#include<stdarg.h>
#include<stdlib.h>
#define LOG(fmt,...) printf("[%s:%d]",fmt,__FILE__,__LINE__,##__VA_ARGS__);
#define _GNU_SOURCE
//不定参函数的使用，不定参数据的访问
void printNum(int count,...)
{
    va_list ap;
    va_start(ap,count);
    int i=0;
    for( i=0;i<count;++i)
    {
        int num=va_arg(ap,int);
        printf("param[%d]:%d\n",i,num);
    }
    va_end(ap);//用完之后置空
}

void myprint(const char* fmt,...)
{
    va_list ap;
    va_start(ap,fmt);
    char* res;
    int ret=vasprintf(&res,fmt,ap);
    if(ret!=-1)
    {
        printf(res);
        free(res);
    }
    va_end(ap);//用完之后置空
}

int main()
{
    LOG("gtt");
    printNum(2,154,26);
    printNum(5,24,8,3,1,0);
    myprint("%s,%d","gtt",6666);
    return 0;
}